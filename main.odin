package main

import "core:fmt"
import "core:os"
import "vendor:stb/image"
import "core:mem"
import "core:path/filepath"
import "core:strings"
import "core:c"
import "core:c/libc"
import "config"
import "core:math"
image_resize_arena : mem.Arena

strings_arena : mem.Arena

main :: proc(){
	using fmt

	config_file_name : string
	//get command line args
	args := os.args
	for arg,i in args{
		println(arg)
		//config file name
		if i == 1{
			config_file_name = arg
		}
	}

	if config_file_name == ""{
		println("no config file name")
		return
	}

	//read config file
	config_file,err := os.open(config_file_name)

	if err != os.ERROR_NONE{
		println("error opening config file")
		return
	}

	config_file_string : string
	//read config file into string
	if config_file_bytes,is_file_read := os.read_entire_file(config_file);is_file_read{
		config_file_string = string(config_file_bytes[:])
	}else{
		//log error
		fmt.println("error reading config file")
	}

	using config
	//parse config file
	data,result_tasks := parse_config(config_file_string)

	for task in result_tasks.tasks{
		fmt.println("Starting Task...")

		t := task

		//input_dir_handle,err := os.open("testdir/in1",os.O_RDONLY)
		input_dir_handle,err := os.open(task.input_dir,os.O_RDONLY)

		fmt.println("Input Dir: ",task.input_dir)
		defer os.close(input_dir_handle)
		if err != os.ERROR_NONE{
			fmt.println("error opening input for task skipping TASK! : ERROR CODE : ",err," ",task.input_dir)
			continue
		}

		//TODO(Ray):Use an arena here
		input_files,erro := os.read_dir(input_dir_handle,0)
		if erro != os.ERROR_NONE{
			fmt.println("error reading directory for task skipping TASK! ERROR: ",erro)
			continue
		}

		get_image_format :: proc(ext : string)-> (result : Image_Format){
			if ext == ".png"{
				result = .IMAGE_FORMAT_PNG
			}else if ext == ".jpg"{
				result = .IMAGE_FORMAT_JPEG
			}else if ext == ".bmp"{
				result = .IMAGE_FORMAT_BMP
			}else if ext == ".qoi"{
				result = .IMAGE_FORMAT_QOI
			}else if ext == ".webp"{
				result = .IMAGE_FORMAT_WEBP
			}else{
				result = .IMAGE_FORMAT_UNKNOWN
			}

			/*else if ext == ".tga"{
				result = Image_Format_TGA
			}else if ext == ".psd"{
				result = Image_Format.PSD
			}else if ext == ".gif"{
				result = Image_Format.GIF
			}else if ext == ".hdr"{
				result = Image_Format.HDR
			}else if ext == ".pic"{
				result = Image_Format.PIC
			}else if ext == ".pnm"{
				result = Image_Format.PNM
			}
			*/

			return 
		}

		is_valid_extension :: proc(task : Batch_Task,format : Image_Format)-> bool{
			for input_format in task.input_formats_allowed{
				//May not end up dealing with this format for now
				if input_format == Image_Format.IMAGE_FORMAT_WEBP{ 
					return false
				}
				if input_format == format{
					return true
				}
			}
			return false
		}

		println("Input Files: ",input_files)
		for input_file in input_files{
			if is_valid_extension(task,get_image_format(filepath.ext(input_file.name))) == false{
				fmt.println("Invalid file type skipping ...")
				continue
			}
			fmt.println("Processing file: " , input_file.name)

			//read image
			w,h,chan,desired_chan : libc.int
			output_channels : i32 = i32(len(task.image_transform.output_channel_format))
			file_path := strings.clone_to_cstring(strings.concatenate([]string{task.input_dir,"/",input_file.name}))
			texels_in_bytes := image.load(file_path,&w,&h,&chan,0)
			
			//determine the output channels
			//only ouput channels that are desired
			//for example b == 1 channel means write the blue channel into the channel 0 of the output and save the file with only that channel meaning 1 8bit channel or 1 32bit channel etc..
			
			if texels_in_bytes == nil{
				fmt.println("error loading image skipping ...")
				continue
			}

			//NOTE(Ray):In the case of no resize we can just overwrite the original texels
			//otherwise we need to allocate new memory
			//iterate over exery texel
			stride := int(w*chan)
			out_stride := int(w*output_channels)
			//NOTE(Ray):We are reading from uninitialized memory on the last row of the image
			//in the case of less than 4 components
			//the results are thrown away so its not an issue algorithmically.
			//TODO(Ray):Tighting things up so we dont need the if's in the channel writes 
			//and than we will do resizing and than the channel swizzling should only be done  
			//if the channels are not the same as input
			//and in the case of swizzling make it multithreadable.
			//We will use off the shelf libraries for the resizing for now
			//but when we might want to optimize it later since most solutions are not optimized for 
			//speed as far as I can tell atm.
			temp_texel : u32
			out_x := 0
			out_y := 0
			for y := 0; y < int(h); y += 1{
				for x := 0; x < stride; x += int(chan){
					out_x += int(output_channels)
					//get texel

					index := ((y * stride) + x)
					texel_ptr := (^u32be)(&texels_in_bytes[index])
					source_texel := (u32)(texel_ptr^)
					dest_index := ((y * out_stride) + out_x)
					dest_texel_ptr := (^u32be)(&texels_in_bytes[dest_index])
					dest_texel := (u32)(dest_texel_ptr^)

					temp_texel = source_texel

					if out_x >= x * int(output_channels){
						out_x = 0
						out_y += 1
					}

					//apply filter
					/*
					EX:
					OutputChannelFormat[0] : Is Alpha channel in source. We get Red:  Move A Source to R dest.
					We need to Shift Alpha to Red channel.

					Calculate the index - offset based on the source channel - dest channel.
					EX:
					Source Channel is Alpha = 24 bit index Dest Channel
					*/
					// An output image with N components has the following components interleaved
					// in this order in each pixel:
					//
					//     N=#comp     components
					//       1           grey
					//       2           grey, alpha
					//       3           red, green, blue
					//       4           red, green, blue, alpha
					//Calculate the shits based on the bit position.

					//create the shifting masks for each channel

					if y < int(h - 1){
						//source_texel = 0x0000FF00
					}
//					testle : u32 = (u32)(source_texel)

//					fmt.println("Source Texel: ",source_texel," ",testle)
					red :=   u32(source_texel & 0xFF000000) >> 24
					green := u32(source_texel & 0x00FF0000) >> 16
					blue  := u32(source_texel & 0x0000FF00) >> 8
					alpha := u32(source_texel & 0x000000FF) >> 0

					src_color : u32 = source_texel
					//dest_color : u32 = dest_texel
					for out_channel,i in task.image_transform.output_channel_format{
						shift : u32
						if int(chan) == i{
							break
						}

						if i == 0{
							shift = 24
						}else if i == 1{
							shift = 16
						}else if i == 2{
							shift = 8
						}else if i == 3{
							shift = 0
						}

						switch out_channel{
							case .Image_Channel_A:
							if chan == 4{
								temp_texel |= alpha << shift
							}else{
								fmt.println("Error: Alpha channel not found in source image")
							}
							case .Image_Channel_R:
							if chan >= 3{
								temp_texel |= red << shift
							}else{
								fmt.println("Error: Red channel not found in source image")
							}
							case .Image_Channel_G:
							if chan >= 2{
								temp_texel |= green << shift
							}else{
								fmt.println("Error: Green channel not found in source image")
							}
							case .Image_Channel_B:
							if chan >= 1{
								temp_texel |= blue << shift
							}else{
								fmt.println("Error: Blue channel not found in source image")
							}
							case .Image_Channel_Count:
							fmt.println("Invalid channel count")
						}
					}
					dest_texel_ptr^ = u32be(temp_texel)
				}
			}

			if task.image_transform.npot_convert == true{
				//convert to npot
				fmt.println("Converting to pot...")

				aspect := f32(w)/f32(h)
				nw := i32(get_next_power_of_two(u32(f32(w))))
				nh := (h * nw) / w
				nstride := int(nw*chan)

				outb,err := mem.alloc(int(nw * nh * chan))
				output_bytes := ([^]u8)(outb)

				error := image.resize_uint8(texels_in_bytes,w,h,libc.int(stride),
				output_bytes,
				libc.int(nw),
				libc.int(nh),
				libc.int(nstride),
				chan)
				fmt.println("Error:",error)
				assert(error == 1)
				output_path := strings.clone_to_cstring(strings.concatenate([]string{task.output_dir,"/",input_file.name}))

				fmt.printf("%v %v %v",nw,nh,nstride)
				error = image.write_png(output_path,libc.int(nw),libc.int(nh),output_channels,output_bytes,libc.int(nstride))
				fmt.println("Error:",error)
				assert(error == 1)
			}if task.image_transform.width != 0 && task.image_transform.height != 0{
				//resize
				fmt.println("resizing..")

				outb,err := mem.alloc(int(task.image_transform.width * task.image_transform.height * int(output_channels)))

				if output_channels < chan{
					multiplier : f32 = f32(chan) / f32(output_channels)
					fmt.println("multiplier:",multiplier)
					h = i32(f32(h) / f32(multiplier))
					fmt.println("resizing..")
				}

				output_bytes := ([^]u8)(outb)
				image.resize_uint8(texels_in_bytes,w,h,libc.int(stride),
				output_bytes,
				libc.int(task.image_transform.width),
				libc.int(task.image_transform.height),
				libc.int(task.image_transform.width) * libc.int(output_channels),
				output_channels)

				output_path := strings.clone_to_cstring(strings.concatenate([]string{task.output_dir,"/",input_file.name}))
				image.write_png(output_path,libc.int(task.image_transform.width),libc.int(task.image_transform.height),output_channels,output_bytes,libc.int(task.image_transform.width) * output_channels)
			}else{
				fmt.println("no valid transformation.. continuing..")
				//no resize
//				output_path := strings.clone_to_cstring(strings.concatenate([]string{task.output_dir,"/",input_file.name}))
//				image.write_png(output_path,w,h,chan,texels_in_bytes,w * chan)
			}
		}

		fmt.println("Ending Task...")
	}

	fmt.println("done")
}

get_next_power_of_two := proc(a : u32)->   u32{
	x := a
	x -= 1
	x |= x >> 1
	x |= x >> 2
	x |= x >> 4
	x |= x >> 8
	x |= x >> 16
	x += 1
	return x
}

closest_power_of_two := proc(num : u32)-> (result : u32){
	using math
	power := u32(math.log2(f32(num)))
	lower : u32 = 1 << power;
	upper : u32 = 1 << (power + 1);
	return (num - lower) < (upper - num) ? lower : upper;
}
