package odin

import "core:text/scanner"
import "core:fmt"
import "core:strings"
import "core:strconv"

File_Info :: struct{
	file : rawptr,
	name : string,
	file_size : int,
}

//Image formats that are supported.
Image_Format :: enum{
	IMAGE_FORMAT_UNKNOWN,
	IMAGE_FORMAT_BMP,
	IMAGE_FORMAT_JPEG,
	IMAGE_FORMAT_PNG,
	IMAGE_FORMAT_WEBP,
	IMAGE_FORMAT_QOI,
//	IMAGE_FORMAT_TGA,
//	IMAGE_FORMAT_GIF,
//	IMAGE_FORMAT_PSD,
//	IMAGE_FORMAT_HDR,
//	IMAGE_FORMAT_PIC,
//	IMAGE_FORMAT_PNM,
}

get_image_format :: proc(s : string)->(result : Image_Format){
	result = .IMAGE_FORMAT_UNKNOWN
	switch s{
		case "bmp"  : result = .IMAGE_FORMAT_BMP
		case "jpg"  : result = .IMAGE_FORMAT_JPEG
		case "jpeg" : result = .IMAGE_FORMAT_JPEG
		case "png"  : result = .IMAGE_FORMAT_PNG
		case "webp" : result = .IMAGE_FORMAT_WEBP
		case "qoi"  : result = .IMAGE_FORMAT_QOI
//		case "tga"  : result = IMAGE_FORMAT_TGA
//		case "gif"  : result = IMAGE_FORMAT_GIF
//		case "psd"  : result = IMAGE_FORMAT_PSD
//		case "hdr"  : result = IMAGE_FORMAT_HDR
//		case "pic"  : result = IMAGE_FORMAT_PIC
//		case "pnm"  : result = IMAGE_FORMAT_PNM
	}
	return
}

ImageChannels :: enum{
	Image_Channel_R= 16, 
	Image_Channel_G = 8,
	Image_Channel_B = 0,
	Image_Channel_A = 24,
	Image_Channel_Count = 4,
}

//This is the channels and size of the image we want to output.
ImageTransform :: struct{
	width : int,
	height : int,
	npot_convert : bool,
	output_channel_format : [dynamic]ImageChannels,
}

get_transform :: proc(s : string)-> (result : ImageTransform){
	ss := strings.split(s, ",")
	for param in ss{
		if strings.contains(param, "width"){
			parse_error := false
			split := strings.split(param, ":")
			if len(split) == 2{
				result.width,parse_error = strconv.parse_int(split[1])
			}else{
				fmt.println("Error parsing width")
			}
		}

		if strings.contains(param, "height"){
			parse_error := false
			split := strings.split(param, ":")
			if len(split) == 2{
				result.height,parse_error = strconv.parse_int(split[1])
			}else{
				fmt.println("Error parsing width")
			}
		}
		if strings.contains(param,"npot_convert"){
			parse_error := false
			split := strings.split(param, ":")
			if len(split) == 2{
				input := strings.trim_space(strings.trim(split[1],"\""))
				leninpu := len(input)
				fmt.println(input)
				fmt.println(leninpu)
				
				assert(leninpu <= 4)
				result.npot_convert,parse_error = strconv.parse_bool(input)
			}else{
				fmt.println("Error parsing width")
			}
		}
		get_channel :: proc(s : rune)->(result : ImageChannels){
			switch s{
				case 'r' : result = .Image_Channel_R
				case 'g' : result = .Image_Channel_G
				case 'b' : result = .Image_Channel_B
				case 'a' : result = .Image_Channel_A
			}
			return
		}

		//TODO(Ray): We will have to revisit here for a variety of different transforms that could be possible 
		//for now this handles the basic cases.
		if strings.contains(param, "channels"){
			result.output_channel_format = make([dynamic]ImageChannels)
			split := strings.split(param, ":")
			if len(split) == 2{
				source_channels := split[1]
				for r in source_channels{
					switch r{
						case 'r' : 
						append(&result.output_channel_format,ImageChannels.Image_Channel_R)
						case 'g' : 
						append(&result.output_channel_format,ImageChannels.Image_Channel_G)
						case 'b' : 
						append(&result.output_channel_format,ImageChannels.Image_Channel_B)
						case 'a' : 
						append(&result.output_channel_format,ImageChannels.Image_Channel_A)
					}
				}
			}
		}
	}
	return
}

//This represents a batch task job.  There can be many of these defined in teh config file.
Batch_Task :: struct{
	name : string,
	dir_files_result : [dynamic]File_Info,
	input_dir : string,
	output_dir : string,
	file_post_fix : string,
	input_formats_allowed : [dynamic]Image_Format,
	output_format : Image_Format,
	image_transform : ImageTransform,
	in_progress : bool,
	output_to_console : bool,
	has_post_fix : bool,
	output_log_dir : string,
}

//This tracks teh progress of our currently running batch task.
Batch_Task_Progress :: struct{
	input_file_count : u32,
	files_to_output_count : u32,
	processed_count : u32,
	processed_percentage : f32,
};

//A set of task to be executed.
Batch_Tasks :: struct{
	tasks : [dynamic]Batch_Task,
	task_in_progress : bool,
}

CFG_Block_Value :: union{
	string,
	int,
	f32,
	bool,
	[dynamic]Image_Format,
	[dynamic]ImageChannels,
	[dynamic]ImageTransform,
	Image_Format,
	ImageChannels,
	ImageTransform,
}

cfg_block_map : map[string]CFG_Block_Value

CFG_Entry :: struct{
	key : string,
	text : [dynamic]string,
};

CFG_Block :: struct{
	entries : [dynamic]CFG_Entry,
}

CFG_Data :: struct{
	blocks : [dynamic]CFG_Block,
}

s_backing : scanner.Scanner

Token :: enum{
	dash = '-',
	underscore = '_',
	period = '.',
	asterisk = '*',
	equals = '=',
	quote = '"',
	slash = '/',
	backslash = '\\',
	semicolon = ';',
	colon = ':',
	comma = ',',
	open_bracket = '[',
	close_bracket = ']',
	open_paren = '(',
	close_paren = ')',
	open_brace = '{',
	close_brace = '}',
	plus = '+',
	minus = '-',
	tilde = '~',
	exclamation = '!',
	ampersand = '&',
	pipe = '|',
	percent = '%',
	less_than = '<',
	greater_than = '>',
	question = '?',
	caret = '^',
	hash = '#',
	whitespace = ' ',
	newline = '\n',
	tab = '\t',
	EOF = -1,
	other = 0,
}

parse_config :: proc(config : string)->  (result : CFG_Data,tasks : Batch_Tasks){
	result.blocks = make([dynamic]CFG_Block)
	tasks.tasks = make([dynamic]Batch_Task)
	//Build blocks
	s := scanner.init(&s_backing,config)
	prev_token : Token
	is_open_block : bool
	for token := scanner.scan(s);token != scanner.EOF;token = scanner.scan(s){
		t := Token(token)
		block : CFG_Block
		if t == .dash && prev_token == .dash{
			fmt.println("Found Open Block")
			is_open_block = true
			block.entries = make([dynamic]CFG_Entry)
		}

		if is_open_block{
			task : Batch_Task
			for token = scanner.scan(s);token != scanner.EOF;token = scanner.scan(s){
				t = Token(token)
				text := scanner.token_text(s)
				if strings.compare(text,"inputdir") == 0{
					entry : CFG_Entry
					entry.key = text
					t = Token(scanner.scan(s))
					if t == .colon{
						t = Token(scanner.scan(s))
						key_text := text
						text = scanner.token_text(s)
						alloc : bool
						task.input_dir,alloc = strings.replace_all(text,"\"","")
						cfg_block_map[key_text] = text
						append(&entry.text,text)
						fmt.println("Found input dir ",text)
						append(&block.entries,entry)
					}else{
						fmt.println("Error: Expected colon after inputdir")
					}
				}

				if strings.compare(text,"outputdir") == 0{
					entry : CFG_Entry
					entry.key = text
					t = Token(scanner.scan(s))
					if t == .colon{
						t = Token(scanner.scan(s))
						key_text := text
						text = scanner.token_text(s)
						alloc : bool
						task.output_dir,alloc = strings.replace_all(text,"\"","")
						cfg_block_map[key_text] = text
						fmt.println("Found output dir ",text)
						append(&entry.text,text)
						append(&block.entries,entry)
					}else{
						fmt.println("Error: Expected colon after input formats")
					}
				}

				if strings.compare(text,"infmt") == 0{
					entry : CFG_Entry
					entry.key = text
					t = Token(scanner.scan(s))
					if t == .colon{
						t = Token(scanner.scan(s))
						value : [dynamic]Image_Format = make([dynamic]Image_Format)
						key_text := text
						text = scanner.token_text(s)
						append(&value,get_image_format(text))
						append(&entry.text,text)
						fmt.println("Found input format ",text)
						for t = Token(scanner.scan(s));t == .comma;t = Token(scanner.scan(s)){
							t = Token(scanner.scan(s))
							text = scanner.token_text(s)
							append(&task.input_formats_allowed,get_image_format(text))
							append(&value,get_image_format(text))
							append(&entry.text,text)
							fmt.println("Found input format ",text)
						}
						cfg_block_map[key_text] = value
						text = scanner.token_text(s)
						append(&block.entries,entry)
					}else{
						fmt.println("Error: Expected colon after input formats")
					}
				}

				if strings.compare(text,"outfmt") == 0{
					t = Token(scanner.scan(s))
					entry : CFG_Entry
					entry.key = text
					key_text := text
					if t == .colon{
						t = Token(scanner.scan(s))
						text = scanner.token_text(s)
						task.output_format = get_image_format(text)
						cfg_block_map[key_text] = get_image_format(text)
						append(&entry.text,text)
						append(&block.entries,entry)
						fmt.println("Found output formats",text)
					}else{
						fmt.println("Error: Expected colon after output formats")
					}
				}

				if strings.compare(text,"transform") == 0{
					t = Token(scanner.scan(s))
					entry : CFG_Entry
					entry.key = text
					key_text := text
					if t == .colon{
						t = Token(scanner.scan(s))
						text = scanner.token_text(s)
						task.image_transform = get_transform(text)
						cfg_block_map[key_text] = text
						append(&entry.text,text)
						append(&block.entries,entry)
						fmt.println("Found transforms",text)
					}else{
						fmt.println("Error: Expected colon after transforms")
					}
				}

				if strings.compare(text,"console") == 0{
					t = Token(scanner.scan(s))
					entry : CFG_Entry
					entry.key = text
					if t == .colon{
						t = Token(scanner.scan(s))
						text = scanner.token_text(s)
						task.output_to_console = true if strings.compare(text,"true") == 0 else false
						cfg_block_map[text] = true if strings.compare(text,"true") == 0 else false
						append(&entry.text,text)
						append(&block.entries,entry)
						fmt.println("Found console",text)
					}else{
						fmt.println("Error: Expected colon after console")
					}
				}

				if strings.compare(text,"filepostfix") == 0{
					t = Token(scanner.scan(s))
					entry : CFG_Entry
					entry.key = text
					if t == .colon{
						t = Token(scanner.scan(s))
						text = scanner.token_text(s)
						task.has_post_fix = true
						task.file_post_fix = text
						cfg_block_map[text] = text
						append(&entry.text,text)
						append(&block.entries,entry)
						fmt.println("Found console",text)
					}else{
						fmt.println("Error: Expected colon after console")
					}
				}

				if strings.compare(text,"outputlogdir") == 0{
					t = Token(scanner.scan(s))
					entry : CFG_Entry
					entry.key = text
					key_text := text
					if t == .colon{
						t = Token(scanner.scan(s))
						text = scanner.token_text(s)
						alloc : bool
						task.output_log_dir,alloc = strings.replace_all(text,"\\","/")
						cfg_block_map[key_text] = text
						append(&entry.text,text)
						append(&block.entries,entry)
						fmt.println("Found outputlogdir",text)
					}else{
						fmt.println("Error: Expected colon after outputlogdir")
					}
				}

				if t == .dash && prev_token == .dash{
					is_open_block = false
					fmt.println("Found Close Block")
					//Setting the previous to other because after a block
					prev_token = .other
					t = .other
					
					append(&result.blocks,block)
					append(&tasks.tasks,task)
					break
				}
				prev_token = t
			}
		}
		prev_token = t
	}
	return
}
